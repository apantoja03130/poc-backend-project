﻿using PoC.APIProject;
using Xunit;

namespace UnitTesting.XUnit.Tests
{
    public class ValidatorShould
    {
        [Fact]
        public void ValidateValidEmails()
        {
            // Arrange
            var weather = new WeatherForecast();
            int temperatureInC = 22;

            // Act
            bool isValid = weather.temperatureIsPositive(temperatureInC);

            // Asert
            Assert.True(isValid, $"{temperatureInC} is not a positive number");
        }
    }
}
